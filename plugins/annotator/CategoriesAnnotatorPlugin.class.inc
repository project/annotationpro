<?php

/**
 * @file
 * Categories Annotator Plugin.
 */

/**
 * Annotator ctools plugin.
 */
class CategoriesAnnotatorPlugin extends AnnotatorPlugin {

  /**
   * Set up CategoriesAnnotatorPlugin.
   */
  public function setup() {
    drupal_add_js(drupal_get_path('module', 'annotationpro') . '/js/annotator_categories.js');
    drupal_add_js(drupal_get_path('module', 'annotationpro') . '/lib/jquery-i18n-master/jquery.i18n.min.js');
    drupal_add_js(drupal_get_path('module', 'annotationpro') . '/js/categories.js');
  }
}
