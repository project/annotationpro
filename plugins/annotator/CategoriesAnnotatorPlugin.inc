<?php

/**
 * @file
 * Categories Annotator Plugin.
 */

$plugin = array(
  'name' => 'categories',
  'label' => t('Categories'),
  'module' => 'annotator',
  'handler' => array(
    'class' => 'CategoriesAnnotatorPlugin',
  ),
);
