/**
 * @file
 * Attaches behaviors for Annotator's Categories plugin.
 */

(function ($) {

  'use strict';

  Drupal.behaviors.annotatorCategories = {
    attach: function (context, settings) {
      Drupal.Annotator.annotator('addPlugin', 'Categories',{
        errata:'annotator-hl-errata',
        destacat:'annotator-hl-destacat',
        subratllat:'annotator-hl-subratllat' }
      );
    }
  };
})(jQuery);
